const spentItems = [
    { "description": "Steam", "value": null, "month": 07 },
    { "description": "Rocketseat", "value": null, "month": 08 },
    { "description": "iTunes", "value": null, "month": 09 },
    { "description": "Youtube Premium", "value": null, "month": 10 },
    { "description": "Uber", "value": 1430, "month": 12 },
    { "description": "PS Store", "value": 40, "month": 03 },
    { "description": "GPlay", "value": 37.50, "month": 03 },
    { "description": "iFood", "value": 77, "month": 02 },
    { "description": "Uber", "value": 3, "month": 01 },
    { "description": "iFood", "value": 34.70, "month": 01 },
    { "description": "Uber", "value": 17.70, "month": 03 },
    { "description": "GPlay", "value": 16.70, "month": 03 },
    { "description": "Appstore", "value": 15, "month": 02 },
    { "description": "Uber", "value": 96.11, "month": 02 },
    { "description": "GPlay", "value": 7.71, "month": 02 },
    { "description": "Appstore", "value": 3.33, "month": 02 },
    { "description": "iFood", "value": 47.98, "month": 03 },
    { "description": "Appstore", "value": 12.22, "month": 03 },
    { "description": "Uber", "value": 2.56, "month": 04 },
    { "description": "Uber", "value": 5.32, "month": 03 },
    { "description": "PS Store", "value": 5.44, "month": 03 },
    { "description": "PS Store", "value": 98.37, "month": 03 },
    { "description": "PS Store", "value": 78.90, "month": 01 },
    { "description": "GPlay", "value": 65.03, "month": 03 },
    { "description": "iFood", "value": 32.12, "month": 03 },
    { "description": "Uber", "value": 34.56, "month": 01 },
    { "description": "iFood", "value": 1480, "month": 03 },
    { "description": "Uber", "value": 5.34, "month": 03 },
    { "description": "iFood", "value": 6.12, "month": 01 },
    { "description": "Uber", "value": 344, "month": 03 },
    { "description": "GPlay", "value": 96.10, "month": 03 },
    { "description": "Uber", "value": 6.09, "month": 01 },
    { "description": "PS Store", "value": 3.21, "month": 03 }
];

const loadTable = () => {
    spentItems.reduce((acumulador, cur) => {
        if (!cur.value) return acumulador;

        const foundMonths = acumulador.find(e => e.month === cur.month);
        if (foundMonths) {
            foundMonths.value += cur.value
        } else {
            acumulador.push({ value: cur.value, month: cur.month })
        }
        
        return acumulador
    }, [])
        .sort((a, b) => b.month - a.month)
        .forEach(item => {
            const tableBody = document.getElementById('tb_fatura').getElementsByTagName('tbody')[0];
            const newRow = tableBody.insertRow(0);

            const monthCell = newRow.insertCell(0);
            monthCell.appendChild(document.createTextNode(getMonthLabel(item.month)));

            const valueCell = newRow.insertCell(1);
            valueCell.appendChild(document.createTextNode(formatNumberInBrlCurrency(item.value)));
        });
};

const formatNumberInBrlCurrency = number => {
    return number.toLocaleString('pt-br', { style: 'currency', currency: 'BRL' })
};

const getMonthLabel = month => {
    switch (month) {
        case 1:
            return "Janeiro"
        case 2:
            return "Fevereiro"
        case 3:
            return "Março"
        case 4:
            return "Abril"
        case 5:
            return "Maio"
        case 6:
            return "Junho"
        case 7:
            return "Julho"
        case 8:
            return "Agosto"
        case 9:
            return "Setembro"
        case 10:
            return "Outubro"
        case 11:
            return "Novembro"
        case 12:
            return "Dezembro"
    };
};

window.onload = () => loadTable();
